let xhr = new XMLHttpRequest();
let urlStr = "./schedule.php?tch_id=" + localStorage.getItem("tch_id");
xhr.open("GET", urlStr);
xhr.send(null);
xhr.onload = () => {
    if (xhr.status == 200) {
        let tBody = document.getElementById("tBodyTch");
        tBody.innerHTML = xhr.responseXML.firstChild.innerHTML;
        let h1 = document.getElementsByTagName("h1")[0];
        h1.innerHTML = "Расписание преподавателя " 
            + localStorage.getItem("tch_name");
    }
}
