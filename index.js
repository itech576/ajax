function initSelect(select, key) {
    let savedValue = localStorage[key];
    if ("undefined" == savedValue || undefined === savedValue) {
         
    } else {
        console.log(JSON.parse(savedValue));
        select.value = JSON.parse(savedValue).val;
    } 
}

let selGr = document.getElementById('groups');
let savedValue = localStorage["gr_id"];
if (savedValue) {
    selGr.value = savedValue;
} 
selGr.onchange = () => {
    let selGr = document.getElementById('groups');
    localStorage.setItem("gr_id", selGr.value);    
};
let btnShowGrSch = document.getElementById("btnShowGrSch");
btnShowGrSch.onclick = () => {
    let grId = localStorage.getItem("gr_id");
    let selTch = document.getElementById('groups');
    localStorage.setItem("gr_title", selGr.options[grId - 1].text);
};

//------------------------------------------------------------------

let selTch = document.getElementById("teachers");
savedValue = localStorage["tch_id"];
if (savedValue) {
    selTch.value = savedValue;
} 
selTch.onchange = () => {
    let selTch = document.getElementById('teachers');
    localStorage.setItem("tch_id", selTch.value);    
};
let btnShowTchSch = document.getElementById("btnShowTchSch");
btnShowTchSch.onclick = () => {
    let tchId = localStorage.getItem("tch_id");
    let selTch = document.getElementById('teachers');
    localStorage.setItem("tch_name", selTch.options[tchId - 1].text);
};


/*
------------------------ select auditorium ------------------------ 
*/
let selAud = document.getElementById("auditoriums");
savedValue = localStorage.getItem("aud");
if (savedValue) {
    selAud.value = savedValue;
}

selAud.onchange = () => {
    let selAud = document.getElementById("auditoriums");
    localStorage.setItem("aud", selAud.value);
};
