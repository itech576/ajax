<?php
    require_once("conn.php");

    if (array_key_exists("gr_id", $_GET)) {
        header("Content-Type: text/html");
            $stm = 
"SELECT l.*, t.name FROM lesson_groups AS lg
    INNER JOIN lesson AS l ON l.ID_Lesson=lg.FID_Lesson2
    INNER JOIN lesson_teacher AS lt ON lt.fid_lesson1=l.id_lesson
    INNER JOIN teacher AS t ON t.id_teacher=lt.fid_teacher
    WHERE lg.fid_groups=:gr_id;";
            
            $pdo_stm = $pdo->prepare($stm);
            $pdo_stm->execute(
                array(':gr_id' => $_GET['gr_id'])
            );
            $pdo_stm->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($pdo_stm as $row) {
                echo '<tr>';
                foreach ($row as $key => $value) {
                    if ($key == "ID_Lesson"){
                        continue;
                    }
                    echo '<td>' . $value . '</td>';
                }
                echo '</tr>';
            }
    } else if (array_key_exists("tch_id", $_GET)) {
            $stm = 
"SELECT l.* FROM teacher AS t
    INNER JOIN lesson_teacher AS lt ON t.id_teacher=lt.fid_teacher
    INNER JOIN lesson AS l ON l.id_lesson=lt.fid_lesson1
    WHERE t.id_teacher=:tch_id;";
            
            $pdo_stm = $pdo->prepare($stm);
            $pdo_stm->execute(
                array(':tch_id' => $_GET['tch_id'])
            );
            $pdo_stm->setFetchMode(PDO::FETCH_ASSOC);

            $stm2 = 
"SELECT g.title FROM lesson AS l 
    INNER JOIN lesson_groups AS lg ON lg.fid_lesson2=l.id_lesson
    INNER JOIN groups AS g ON g.id_groups=lg.fid_groups
    WHERE l.ID_Lesson=:les_id;";
            $pdo_stm2 = $pdo->prepare($stm2);
            $pdo_stm2->setFetchMode(PDO::FETCH_ASSOC);
        
            header('Content-Type: text/xml');
            header("Cache-Control: no-cache, must-revalidate");
            echo '<?xml version="1.0" encoding="utf8" ?>';
            echo "<root>";
            
            foreach ($pdo_stm as $row) {
                echo '<tr>';
                foreach ($row as $key => $value) {
                    if ($key == "ID_Lesson"){
                        continue;
                    }
                    echo '<td>' . $value . '</td>';
                }
                                
                $pdo_stm2->execute(
                    array(':les_id' => $row['ID_Lesson'])
                );

                echo '<td>';
                foreach ($pdo_stm2 as $group) {
                    echo $group['title'] . ' ';
                }
                echo '</td>';
                
                echo '</tr>';
            }   
            
            echo "</root>";
    } else if (array_key_exists("aud", $_GET)) {
        header("Content-Type: application/json");
        
            $stm = 
"SELECT t.name, l.* FROM lesson AS l
    INNER JOIN lesson_teacher AS lt ON lt.fid_lesson1=l.id_lesson
    INNER JOIN teacher AS t ON t.id_teacher=lt.fid_teacher
    WHERE l.auditorium LIKE :aud";
            
            $pdo_stm = $pdo->prepare($stm);
            $pdo_stm->execute(
                array(':aud' => $_GET['aud'])
            );
            $pdo_stm->setFetchMode(PDO::FETCH_ASSOC);

            $stm2 = 
"SELECT g.title FROM lesson AS l 
    INNER JOIN lesson_groups AS lg ON lg.fid_lesson2=l.id_lesson
    INNER JOIN groups AS g ON g.id_groups=lg.fid_groups
    WHERE l.ID_Lesson=:les_id;";
            $pdo_stm2 = $pdo->prepare($stm2);
            $pdo_stm2->setFetchMode(PDO::FETCH_ASSOC);
            
            echo '[';
            foreach ($pdo_stm as $row) {
                echo '{';
                foreach ($row as $key => $value) {
                    if ($key == "ID_Lesson"){
                        continue;
                    }
                    echo '"' . $key . '": "' . $value . '",';
                }
                                
                $pdo_stm2->execute(
                    array(':les_id' => $row['ID_Lesson'])
                );

                echo '"groups": "';
                foreach ($pdo_stm2 as $group) {
                    echo $group['title'] . ', ';
                }
                echo '"';
                
                echo '},';
            }        
            echo ' null]';
    }