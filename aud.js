async function async_func() {
    let urlStr = "./schedule.php?aud=" + localStorage.getItem("aud");
    let response = await fetch(urlStr);
    let json = await response.json();
    
    let h1 = document.getElementsByTagName("h1")[0];
    h1.innerHTML = "Расписание аудитории " 
                        + localStorage.getItem("aud");

    let tBodyAud = document.getElementById("tBodyAud");  
    for (lesson of json) {
        if (null == lesson)
            continue;
        let row = tBodyAud.insertRow(-1);
        row.insertCell(0).innerHTML = lesson.name;
        row.insertCell(1).innerHTML = lesson.week_day;
        row.insertCell(2).innerHTML = lesson.lesson_number;
        row.insertCell(3).innerHTML = lesson.disciple;
        row.insertCell(4).innerHTML = lesson.type;
        let groupsStr = lesson.groups;
        row.insertCell(5).innerHTML
                = groupsStr.substr(0, groupsStr.length - 2);
    }    
};

async_func();