<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Shcedule</title>
    <link href="style.css" rel="stylesheet">
    <script src="index.js" defer></script>
</head>
<body>
   <h1>Расписание</h1>
        <label for="groups">Выберите группу:</label>
        <select id="groups" name="gr_id">
    
    <?php  
    require_once 'conn.php';
            
    $stm = "SELECT * FROM groups;";    
    foreach ($pdo->query($stm) as $row) {
        echo "<option value=\"" 
            . $row['ID_Groups'] 
            . "\">" . $row['title']
            . "</option>";
    }
    ?>
         </select>        
        <a href="group.htm">
            <button id="btnShowGrSch">Показать</button>
        </a>
    <br>
    <br>   
        <label for="teachers">
            Выберите преподавателя:
        </label>
        <select id="teachers" name="tch_id">
    <?php       
    $stm = "SELECT * FROM teacher;";    
    foreach ($pdo->query($stm) as $row) {
        echo "<option value=\"" 
            . $row['ID_Teacher'] 
            . "\">" . $row['name']
            . "</option>";
    }
    ?>
         </select>
         <a href="tch.htm">
             <button id="btnShowTchSch">Показать</button>
         </a>
    <br>
    <br>       
        <label for="auditoriums">
            Выберите аудиторию:
        </label>
        <select id="auditoriums" name="aud">
            <?php
                $stm = 
"SELECT DISTINCT auditorium FROM lesson";
                foreach ($pdo->query($stm) as $row) {
                    $aud = $row['auditorium'];
                    echo "<option value=\"$aud\">"
                        . $aud . "</option>";
                }
            ?>
        </select>
        <a href="aud.htm">
            <button>Показать</button>
        </a>
</body>
</html>